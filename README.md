# EFK


## **E**lasticsearch

### 有用的連結
  - 官方說明文件：
    https://www.elastic.co/guide/en/elasticsearch/reference/current/
  - 官方 Docker：
    https://www.docker.elastic.co/r/elasticsearch
  - Docker Hub：
    https://hub.docker.com/r/elastic/elasticsearch

---

## **F**luentd/**F**luent Bit

### 有用的連結
  - 官方說明文件：
    https://docs.fluentbit.io/manual/
  - Docker Hub：
    https://hub.docker.com/r/fluent/fluent-bit

### 主設定檔位置
  - Ubuntu apt-get：
    /etc/td-agent-bit/td-agent-bit.conf
  - Docker：
    /fluent-bit/etc/fluent-bit.conf

---

## **K**ibana

### 有用的連結
  - 官方說明文件：
    https://www.elastic.co/guide/en/kibana/current/
  - 官方 Docker：
    https://www.docker.elastic.co/r/kibana
  - Docker Hub：
    https://hub.docker.com/r/elastic/kibana